<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// show overall standings of the teams
Route::get('/','StandingsController@showStandings')->name('standings');

Route::get('/results','StandingsController@showStandings')->name('results');
