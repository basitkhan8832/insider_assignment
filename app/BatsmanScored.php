<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatsmanScored extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];

    public function match(){

    	return $this->belongsTo(Player::class);
    }
}
