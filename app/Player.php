<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];

    public function team(){

    	return $this->belongsTo(Team::class);
    }
    public function matches(){

    	return $this->hasMany(Match::class,'man_of_match');
    }
    public function playerOut(){

    	return $this->hasMany(Wicketstaken::class,'player_out');
    }

    public function striker(){

        return $this->hasMany(BallByBall::class,'striker');
    }
    public function nonStriker(){

        return $this->hasMany(BallByBall::class,'non_striker');
    }
    public function bowler(){

        return $this->hasMany(BallByBall::class,'bowler');
    }
}
