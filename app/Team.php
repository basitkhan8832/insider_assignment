<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];

    public function player(){

    	return $this->hasMany(Team::class);
    }

    public function teamOne(){

    	return $this->hasMany(Match::class,'team_one_id');
    }
    public function teamtwo(){

    	return $this->hasMany(Match::class,'team_two_id');
    }
    public function tossWinner(){

    	return $this->hasMany(Match::class,'toss_winner');
    }
    public function matchWinner(){

    	return $this->hasMany(Match::class,'match_winner');
    }

}