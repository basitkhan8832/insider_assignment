<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * List all results.
     *
     * @return mixed
     */
    public function showResults()
    {
        return view('RoundResults.index');
    }
}
