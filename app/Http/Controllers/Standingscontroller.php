<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StandingsController extends Controller
{
    /**
     * List all standings.
     *
     * @return mixed
     */
    public function showStandings()
    {
        return view('Standings.index');
    }
}
