<?php
namespace App\Repositories;

interface PlayersRepositoryInterface
{

    /**
     * Get's all player.
     *
     * @return mixed
     */
    public function all();
        /**
     * Get single player.
     *
     * @return mixed
     */
    public function show($id);
        /**
     * update player.
     *
     * @return mixed
     */
    public function update(array $data,$id);
        /**
     * store player.
     *
     * @return mixed
     */
    public function store(array $data);
        /**
     * delete player.
     *
     * @return mixed
     */
    public function destroy($id);
}