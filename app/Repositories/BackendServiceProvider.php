<?php
namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\StandingsRepositoryInterface',
            'App\Repositories\StandingsRepository'
        );
         $this->app->bind(
            'App\Repositories\TeamsRepositoryInterface',
            'App\Repositories\TeamsRepository'
        );
        $this->app->bind(
            'App\Repositories\PlayersRepositoryInterface',
            'App\Repositories\PlayersRepository'
        );
        $this->app->bind(
            'App\Repositories\MatchesRepositoryInterface',
            'App\Repositories\MatchesRepository'
        );
    }
}