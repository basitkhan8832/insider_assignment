<?php
namespace App\Repositories;

interface TeamsRepositoryInterface
{

    /**
     * Get's all teams.
     *
     * @return mixed
     */
    public function all();
        /**
     * Get single team.
     *
     * @return mixed
     */
    public function show($id);
        /**
     * update team.
     *
     * @return mixed
     */
    public function update(array $data,$id);
        /**
     * store team.
     *
     * @return mixed
     */
    public function store(array $data);
        /**
     * delete team.
     *
     * @return mixed
     */
    public function destroy($id);
}