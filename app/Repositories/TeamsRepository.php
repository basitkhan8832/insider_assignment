<?php
namespace App\Repositories;

use App\Team;

class TeamsRepository implements TeamsRepositoryInterface
{
    
    public function all()
    {
        return Team::all();
    }
    public function show($id)
    {
        return Team::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $team = Team::findOrFail($id);
        $team->update($request->all());

        return $team;
    }

    public function store(Request $request)
    {
        $team = Team::create($request->all());
        return $team;
    }

    public function destroy($id)
    {
        $team = Team::findOrFail($id);
        $team->delete();
        return '';
    }

}