<?php
namespace App\Repositories;

use App\Match;

class MatchesRepository implements MatchesRepositoryInterface
{
    
    public function all()
    {
        return Match::all();
    }
    public function show($id)
    {
        return Match::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $match = Match::findOrFail($id);
        $match->update($request->all());

        return $match;
    }

    public function store(Request $request)
    {
        $match = Match::create($request->all());
        return $match;
    }

    public function destroy($id)
    {
        $match = Match::findOrFail($id);
        $match->delete();
        return '';
    }

}