<?php
namespace App\Repositories;

use App\Player;

class PlayersRepository implements PlayersRepositoryInterface
{
    
    public function all()
    {
        return Player::all();
    }
    public function show($id)
    {
        return Player::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $player = Player::findOrFail($id);
        $player->update($request->all());

        return $player;
    }

    public function store(Request $request)
    {
        $player = Player::create($request->all());
        return $player;
    }

    public function destroy($id)
    {
        $player = Player::findOrFail($id);
        $player->delete();
        return '';
    }

}