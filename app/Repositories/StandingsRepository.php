<?php
namespace App\Repositories;

use App\Match;
use DB;
class StandingsRepository implements StandingsRepositoryInterface
{
    
    /**
     * Get's all macthes.
     *
     * @return mixed
     */
    public function all()
    {
    	return DB::select(DB::raw(" 
		    SELECT t.name, (select COUNT(id) from matches) as Total, (select COUNT(match_winner) from matches WHERE match_winner = t.id) as Won, (select COUNT(match_losser) from matches WHERE match_losser = t.id) as Lose from teams as t"));
    }

}