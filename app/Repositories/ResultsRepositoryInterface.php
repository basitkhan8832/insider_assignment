<?php
namespace App\Repositories;

interface ResultsRepositoryInterface
{

    /**
     * Get's teams inngs.
     *
     * @return mixed
     */
    public function teamInngs($teamid);
    
}