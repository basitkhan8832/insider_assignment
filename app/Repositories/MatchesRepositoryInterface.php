<?php
namespace App\Repositories;

interface MatchesRepositoryInterface
{

    /**
     * Get's all matches.
     *
     * @return mixed
     */
    public function all();
        /**
     * Get single match.
     *
     * @return mixed
     */
    public function show($id);
        /**
     * update match.
     *
     * @return mixed
     */
    public function update(array $data,$id);
        /**
     * store match.
     *
     * @return mixed
     */
    public function store(array $data);
        /**
     * delete match.
     *
     * @return mixed
     */
    public function destroy($id);
}