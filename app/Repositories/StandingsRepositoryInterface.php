<?php
namespace App\Repositories;

interface StandingsRepositoryInterface
{

    /**
     * Get's all standings.
     *
     * @return mixed
     */
    public function all();
}