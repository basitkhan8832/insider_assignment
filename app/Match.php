<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];

    public function player(){

    	return $this->belongsTo(Player::class,'man_of_match');
    }
    public function teamtwo(){

    	return $this->belongsTo(Team::class,'team_two_id');
    }
    public function tossWinner(){

    	return $this->belongsTo(Team::class,'toss_winner');
    }
    public function matchWinner(){

    	return $this->belongsTo(Team::class,'match_winner');
    }

    public function playerScored(){

        return $this->belongsTo(BatsmanScored::class);
    }

    public function ballByBall(){

        return $this->hasMany(BallByBall::class);
    }


}
