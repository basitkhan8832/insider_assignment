<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraRuns extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];
}
