<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BallByBall extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];

    public function match(){

    	return $this->belongsTo(Match::class);
    }

    public function teamBatting(){

    	return $this->belongsTo(Team::class,'team_batting');
    }
    public function teamBowling(){

    	return $this->belongsTo(Team::class,'team_bowling');
    }
    public function striker(){

    	return $this->belongsTo(Player::class,'striker');
    }
    public function nonStriker(){

    	return $this->belongsTo(Player::class,'non_striker');
    }
    public function bowler(){

    	return $this->belongsTo(Player::class,'bowler');
    }
}
