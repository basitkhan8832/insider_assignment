<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wicketstaken extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];


    public function player(){

    	return $this->belongsTo(Player::class,'player_out');
    }
}
