<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('venue',100);
            $table->integer('total_overs');
            $table->integer('team_one_id')->unsigned();
            $table->integer('team_two_id')->unsigned();
            $table->integer('toss_winner')->unsigned();
            $table->string('toss_decision');
            $table->string('win_type');
            $table->string('win_margin');
            $table->integer('match_winner')->unsigned();
            $table->integer('match_losser')->unsigned();
            $table->integer('man_of_match')->unsigned();
            $table->dateTime('match_date_time');
            $table->enum('status', ['active', 'disable'])->default('active');
            $table->timestamps();

            $table->foreign('team_one_id')->references('id')->on('teams');
            $table->foreign('team_two_id')->references('id')->on('teams');
            $table->foreign('toss_winner')->references('id')->on('teams');
            $table->foreign('match_winner')->references('id')->on('teams');
            $table->foreign('match_losser')->references('id')->on('teams');
            $table->foreign('man_of_match')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
