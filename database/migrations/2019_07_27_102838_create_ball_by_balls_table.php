<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallByBallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_by_balls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned();
            $table->integer('over_id');
            $table->integer('innings_no');
            $table->integer('team_batting')->unsigned();
            $table->integer('team_bowling')->unsigned();
            $table->integer('striker_batting_position')->unsigned();
            $table->integer('striker')->unsigned();
            $table->integer('non_striker')->unsigned();
            $table->integer('bowler')->unsigned();
            $table->enum('status', ['active', 'disable'])->default('active');
            $table->timestamps();

            $table->foreign('match_id')->references('id')->on('matches');
            $table->foreign('team_batting')->references('id')->on('teams');
            $table->foreign('team_bowling')->references('id')->on('teams');
            $table->foreign('striker')->references('id')->on('players');
            $table->foreign('non_striker')->references('id')->on('players');
            $table->foreign('bowler')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_by_balls');
    }
}
