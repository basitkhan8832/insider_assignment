<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWicketstakensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wicketstakens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned();
            $table->integer('over_id');
            $table->integer('ball_id');
            $table->integer('player_out')->unsigned();
            $table->string('player_out_type');
            $table->integer('innings_no');
            $table->enum('status', ['active', 'disable'])->default('active');
            $table->timestamps();

             $table->foreign('match_id')->references('id')->on('matches');
             $table->foreign('player_out')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wicketstakens');
    }
}
